import {render, html, svg} from 'https://unpkg.com/uhtml?module';
import { initVideoCamera } from "./initVideoCamera.js"
import { openBackground } from "./openBackground.js"
import * as draw from "./drawPatterns.js";
import { drawRaw } from "./drawRaw.js";

const state = {
	reference: null,
	background: null,
	backgroundDrawing: "blank", // lines, crosses
	y: 450,
	lineWidth: 10,
	download: false,
	threshold: 70,
	overlay: false,
	camera: {
		focalLength: 1460,
		width: 1920,
		height: 1080,
	},
	projector: {
		focalLength: 1750,
		width: 1280,
		height: 720,
	}
}

const backgrounds = {
	"blank": () => {
		const nwd = state.background;
		const nwdContainer = nwd.querySelector(".container");
		const nwdCanvas = nwd.querySelector(".scan-window");
		const nwdCtx = nwdCanvas.getContext("2d");
		const { width, height } = nwdContainer.getBoundingClientRect();
	  nwdCanvas.width = width;	
	  nwdCanvas.height = height;
	  nwdCtx.fillStyle = "black";
		nwdCtx.fillRect(0, 0, width, height);
	},
	"vertical-lines": () => {
		const nwd = state.background;
		const nwdContainer = nwd.querySelector(".container");
		const nwdCanvas = nwd.querySelector(".scan-window");
		const nwdCtx = nwdCanvas.getContext("2d");
		const { width, height } = nwdContainer.getBoundingClientRect();
	  nwdCanvas.width = width;	
	  nwdCanvas.height = height;
		draw.verticalLines(width, height, nwdCtx);
	},
	"gaussian": () => {
		const nwd = state.background;
		const nwdContainer = nwd.querySelector(".container");
		const nwdCanvas = nwd.querySelector(".scan-window");
		const nwdCtx = nwdCanvas.getContext("2d");
		const { width, height } = nwdContainer.getBoundingClientRect();
	  nwdCanvas.width = width;	
	  nwdCanvas.height = height;
		const stdev = 10;
		draw.gaussian(nwdCtx, state.y, width, height, stdev, stdev*5);
	},
	"crosses": () => {
		const nwd = state.background;
		const nwdContainer = nwd.querySelector(".container");
		const nwdCanvas = nwd.querySelector(".scan-window");
		const nwdCtx = nwdCanvas.getContext("2d");
		const { width, height } = nwdContainer.getBoundingClientRect();
	  nwdCanvas.width = width;	
	  nwdCanvas.height = height;
		draw.crosses(width, height, nwdCtx);
	},
	"rectangle":() => {
		const nwd = state.background;
		const nwdContainer = nwd.querySelector(".container");
		const nwdCanvas = nwd.querySelector(".scan-window");
		const nwdCtx = nwdCanvas.getContext("2d");
		const { width, height } = nwdContainer.getBoundingClientRect();
	  nwdCanvas.width = width;	
	  nwdCanvas.height = height;
	  nwdCtx.fillStyle = "black";
		nwdCtx.fillRect(0, 0, width, height);
		nwdCtx.fillStyle = "white";
		nwdCtx.fillRect(0, state.y - state.lineWidth/2, nwdCanvas.width, state.lineWidth);
	},
}

// <button @click=${() => initCanvas()}>init canvas</button>
// 		<button @click=${() => crosses()}>draw crosses</button>
// 		<button @click=${() => verticalLines()}>vertical lines</button>
// 		<button @click=${() => scan()}>scan</button>
// 		<button @click=${() => snapshot(state)}>snapshot</button>

const view = (state) => html`
	<div class="canvas-containers">
		<video id="video" width="1920" style="display:none;"></video>
		<canvas id="raw-canvas" @click=${getMouseCoordinates}></canvas>
		<canvas id="bg-canvas"></canvas>
		<canvas id="output-canvas"></canvas>
	</div>
	<div class="toolbox">
		<button @click=${() => {
			const video = document.querySelector("video");
			const canvas = document.querySelector("#raw-canvas");
			initVideoCamera(video);
			drawRaw(canvas, video, state);
		}}>init video</button>
		<button @click=${async () => { 
			const canvas = document.querySelector("#raw-canvas");
			const ctx = canvas.getContext("2d");
			state.reference = ctx.getImageData(0, 0, canvas.width, canvas.height);

			const bgCanvas = document.querySelector("#bg-canvas");
			const bgCtx = bgCanvas.getContext("2d");
			bgCanvas.width = canvas.width;
			bgCanvas.height = canvas.height;
			bgCtx.putImageData(state.reference, 0, 0);

			const outCanvas = document.querySelector("#output-canvas");
			outCanvas.width = canvas.width;
			outCanvas.height = canvas.height;
			snapshot(state);
		}}>set camera reference</button>
		<button @click=${() => { 
			state.background = openBackground();
		}}>open background</button>
		<button @click=${() => { 
			backgrounds[state.backgroundDrawing]();
		}}>draw background</button>
		<span>
			Background: 
			<select @change=${e => {

				const chosen = e.target.value;
				state.backgroundDrawing = chosen;
				if (chosen in backgrounds) backgrounds[chosen]();
				else console.log("unknown background:", chosen);
			}}>
				<option value="blank">blank</option>
		    <option value="vertical-lines">vertical-lines</option>
		    <option value="crosses">crosses</option>
		    <option value="gaussian">gaussian</option>
		    <option value="rectangle">rectangle</option>
			</select>
		</span>
		<span>
			Y-Value:  <input 
				type="number" 
				@change=${(e) => changeY(e)}
				.value=${state.y}/>
		</span>
		<span>
			Overlay Background: <input 
				type="checkbox" 
				@change=${() => {state.overlay = !state.overlay} } 
				.value="${state.overlay}"/>
		</span>
		<span>
			Download: <input 
				type="checkbox" 
				@change=${() => {state.download = !state.download} } 
				.value="${state.download}"/>
		</span>
		<button @click=${() => scanDownload()}>scan and download</button>
		<button @click=${() => getHeight()}>process</button>
	</div>
`

render(document.body, view(state));

function getMouseCoordinates(e) {
	const canvas = document.querySelector("#raw-canvas");
	const rect = canvas.getBoundingClientRect();
  const x = e.clientX - rect.left;
  const y = e.clientY - rect.top;
  console.log(x, y);
  return {x, y}
}

function sleep(ms = 0) {
  return new Promise(r => setTimeout(r, ms));
}

function snapshot({ camera, threshold, reference }) {
	const width = camera.width;
	const height = camera.height;
	const rawCtx = document.querySelector("#raw-canvas").getContext("2d");
	const frame = rawCtx.getImageData(0, 0, width, height);

	var l = frame.data.length / 4;

	for (var i = 0; i < l; i++) {
		const bg = (reference.data[i * 4 + 0] + reference.data[i * 4 + 1] + reference.data[i * 4 + 2]) / 3;
		const grey = (frame.data[i * 4 + 0] + frame.data[i * 4 + 1] + frame.data[i * 4 + 2]) / 3;
		const result = (grey - bg) > threshold ? 255 : 0;

		frame.data[i * 4 + 0] = result;
		frame.data[i * 4 + 1] = result;
		frame.data[i * 4 + 2] = result;
	}

  document.querySelector("#output-canvas").getContext("2d").putImageData(frame, 0, 0);

  window.
}

// frep -> gerber -> osh park
// stencil font


async function changeY(e) {
	state.y = Number(e.target.value);
	scan(state.y);
	await sleep(500);
	snapshot(state);
}



async function scanDownload(e) {
	const pts = [];
	for (let i = 50; i < 500; i++) {
		scan(i);
		await sleep(50);
		state.y = i;
		snapshot(state);
		pts.push(...getHeight());
		// snapshot({ ...state, y: i });
	}

	const n = pts.length;

	const ply = `ply
format ascii 1.0
element vertex ${n}
property float32 x
property float32 y
property float32 z
end_header
${pts.map(([x, y, z]) => `${x} ${y} ${z}`).join("\n")}
`

	console.log(ply);
}

function getMeanHeight(arr) {
	let possiblePixels = [];

	for (let i = 0; i < arr.length; i++) {
		const px = arr[i];

		if (possiblePixels.length === 0) possiblePixels.push(px);
		else if (possiblePixels[possiblePixels.length - 1] - px === 1) possiblePixels.push(px);
		else if (possiblePixels.length >= 4) break;
		else possiblePixels = [];
	}

	return possiblePixels.length >= 4 ? possiblePixels.reduce((acc, cur) => acc + cur, 0)/possiblePixels.length : 0;
}

function getHeight() {
	var image = oCtx.getImageData(0, 0, outputCanvas.width, outputCanvas.height);
	var l = image.data.length / 4;
	const rows = image.height;
	const cols = image.width;
	// let test = new Uint8ClampedArray(cols*rows*4);
	const whitecounts = [];
	for (let i = 0; i < cols; i++) {
		let whitecount = [];
		for (let j = rows-1; j >= 0; j--) {
			const index = (i+j*cols)*4

			const px = image.data[index+1];

			if (px === 255) whitecount.push(j);
		}

		whitecounts.push(getMeanHeight(whitecount));
		
	}	

	const points = [];

	oCtx.fillStyle = "red";
	for (let i = 0; i < whitecounts.length; i++) {
		if (whitecounts[i] === 0) continue;
		oCtx.beginPath();
		oCtx.arc(i, whitecounts[i], 2, 0, 2 * Math.PI);
		oCtx.fill();
		const x = i - (image.width-1)/2;
		const y = whitecounts[i] - (image.height-1)/2;
		const norm = Math.sqrt(x**2+y**2+state.camera.focalLength**2);
		points.push([ x/norm, y/norm, state.camera.focalLength/norm ]);
	}

	const projectorPt = [ 0, state.y - state.projector.height/2, state.projector.focalLength ];
	const projectorPtNorm = Math.sqrt(projectorPt[0]**2+projectorPt[1]**2+projectorPt[2]**2);
	const projectorPtNormal = [ 
		0/projectorPtNorm, 
		(state.y - state.projector.height/2)/projectorPtNorm, 
		state.projector.focalLength/projectorPtNorm
	];	



	const cameraPos = [7.14586212, -81.71286646, 76.40578868];
	// const cameraPos = [0, -81.71286646, 0];

	// what about camera rotation?
	const realSpacePts = points.map((u, i) => {
		return isect_line_plane_v3(cameraPos, u, [0, 0, 0], projectorPtNormal, [1, 0, 0]);
	});

	return realSpacePts;
}

function crossProduct(v0, v1) {
	return [
		v0[1]*v1[2] - v0[2]*v1[1],
		-(v0[0]*v1[2] - v0[2]*v1[0]),
		v0[0]*v1[1] - v0[1]*v1[0],
	]
}

function isect_line_plane_v3(cameraPos, cameraNormal, planePos, planeNormal0, planeNormal1, epsilon=1e-6) {
	const crossNormals = crossProduct(planeNormal0, planeNormal1);
	const numerator = dot(crossNormals, sub(cameraPos, planePos))
	const denominator = dot(neg(cameraNormal), crossNormals);
	const t = numerator/denominator;

	return add(cameraPos, mul(cameraNormal, t));
}


function add(v0, v1) {
    return [
        v0[0] + v1[0],
        v0[1] + v1[1],
        v0[2] + v1[2],
    ]

}

function neg(v) {
    return v.map(x => -x);
}

function sub(v0, v1) {
    return [
        v0[0] - v1[0],
        v0[1] - v1[1],
        v0[2] - v1[2],
    ]

}

function dot(v0, v1) {
    return (
        (v0[0] * v1[0]) +
        (v0[1] * v1[1]) +
        (v0[2] * v1[2])
    )
}


function len_squared(v0) {
    return dot_v3v3(v0, v0)
}


function mul(v0, f) {
    return [
        v0[0] * f,
        v0[1] * f,
        v0[2] * f,
    ]
}

// all in pixel units
const makeMatrix = ({ focalLength, width, height }) => [
	[focalLength, 0, (width-1)/2],
	[0, focalLength, (height-1)/2],
	[0, 0, 1]
]

const cameraMatrix = makeMatrix(state.camera);
const projectorMatrix = makeMatrix(state.projector);


