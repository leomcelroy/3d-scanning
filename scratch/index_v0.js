const video = document.getElementById('video');
const image = document.getElementById("image");

const bgCanvas = document.querySelector("#bg-canvas");
const outputCanvas = document.querySelector("#output-canvas");
const scanCanvas = document.querySelector("#scan-canvas");
const bgCtx = bgCanvas.getContext("2d");
const oCtx = outputCanvas.getContext("2d");
const slCtx = scanCanvas.getContext("2d");

const initButtonVideo = document.querySelector("#init-button-video");
const initButtonCanvas = document.querySelector("#init-button-canvas");
const snapshotButton = document.querySelector("#snapshot-button");
const scanButton = document.querySelector("#scan-button");

const state = {
	stream: null,
	imageCapture: null,
	width: 0,
	height: 0,
	background: null,
	y: 30,
	lineWidth: 10,
	// started: false
}

const initVideo = async () => {
	let videoConstraints = {
	  frameRate: 20,
	};

	// let videoConstraints = {width: 360, frameRate: 20};
	const mediaDevices = await navigator.mediaDevices;
	const devices = (await mediaDevices.enumerateDevices()).filter( x => x.label.includes("USB Camera"));
	console.log(devices[0]) // specific camera

	return mediaDevices.getUserMedia({
	  // audio: {"echoCancellation": true},
	  video: videoConstraints,
	});
}

function download(name) {
	var imageData = canvas.toDataURL();

    // create temporary link  
    var tmpLink = document.createElement('a');  
    tmpLink.download = `${name}.png`; // set the name of the download file 
    tmpLink.href = imageData;  
      
    // temporarily add link to body and initiate the download  
    document.body.appendChild( tmpLink );  
    tmpLink.click();  
    document.body.removeChild( tmpLink );
}

function initializeDrawing() {
    const w = canvas.width;
    const h = canvas.height;

    let background = undefined;
    let step = 0;
    function draw() {
      // save the first few video frames here
      ctx.drawImage(video, 0, 0, w, h);
      var frame = ctx.getImageData(0, 0, w, h);
      if (step < 2) {
        background = ctx.getImageData(0, 0, w, h);
      }
      var l = frame.data.length / 4;


      for (var i = 0; i < l; i++) {
      	if (step < 3) break;
        const bg = (background.data[i * 4 + 0] + background.data[i * 4 + 1] + background.data[i * 4 + 2]) / 3;
        const grey = (frame.data[i * 4 + 0] + frame.data[i * 4 + 1] + frame.data[i * 4 + 2]) / 3;
        // var val = grey > 124 ? 0 : 255;
        // if (i % 1000 === 0) console.log(background, grey, bg);
        // var val = bg - grey > 30 ? 255 : 0;
        const result = (grey - bg) > 120 ? 255 : 0;
  
        frame.data[i * 4 + 0] = result;
        frame.data[i * 4 + 1] = result;
        frame.data[i * 4 + 2] = result;
      }

      ctx.putImageData(frame, 0, 0);
      if (state.started) download(step);
      
      step++;

    }

    setInterval(draw, 1000/10);
}


const start = async () => {
	const stream = await initVideo();
	state.stream = stream;
	video.srcObject = stream;
	console.log(navigator.mediaDevices.getSupportedConstraints());
	setUpBackground();

	// ctx.drawImage(video, 0, 0, w, h);

	// const track = stream.getVideoTracks()[0];
 	// const imageCapture = new ImageCapture(track);
 	// state.imageCapture = imageCapture;

    // setTimeout(() => {
    //   const { width, height } = video.getBoundingClientRect();
    //   canvas.width = width;
    //   canvas.height = height;
    //   initializeDrawing();
    // }, 2000)




	// navigator.mediaDevices.getUserMedia({ video: true })
	//   .then(mediaStream => {
	//     // Do something with the stream.
	//     const track = mediaStream.getVideoTracks()[0];
	//     const capabilities = track.getCapabilities();
	//     // const constraints = track.getSupportedConstraints();

	//     console.log(capabilities);
	//   })
}	

window.addEventListener("keydown", async (e) => {
   // if (e.key === " ") {
   // 	    const imageBlob = await state.imageCapture.takePhoto();
   //  	image.src = URL.createObjectURL(imageBlob);
   //  	image.onload = () => { URL.revokeObjectURL(image.src); }
   // }
})

function onInitVideo() {
	console.log("init");
	start();
}

initButtonVideo.addEventListener("click", onInitVideo)

function setUpBackground() {
	// open pop up window
	const html = `
		<style>
			html, body {
				margin: 0px;
			}

			.container {
				width: 100vw;
				height: 100vh;
			}
		</style>
		<div class="container">
			<canvas class="scan-window"></canvas>
		</div>
	`

	state.scanWindow = window.open("", "", "width=200, height=200");
	const nwd = state.scanWindow.document;
	nwd.open()
    nwd.write(html)
    nwd.close()
}

function onInitCanvas() {
	const { width, height } = video.getBoundingClientRect();
	bgCanvas.width = width;
	bgCanvas.height = height;
	outputCanvas.width = width;
	outputCanvas.height = height;
	scanCanvas.width = width;
	scanCanvas.height = height;
	state.width = width;
	state.height = height;

	bgCtx.drawImage(video, 0, 0, state.width, state.height);

	state.background = bgCtx.getImageData(0, 0, state.width, state.height);

	snapshot();
}

initButtonCanvas.addEventListener("click", onInitCanvas);

function snapshot() {
	oCtx.drawImage(video, 0, 0, state.width, state.height);
	const frame = oCtx.getImageData(0, 0, state.width, state.height);

	var l = frame.data.length / 4;

	for (var i = 0; i < l; i++) {
		const bg = (state.background.data[i * 4 + 0] + state.background.data[i * 4 + 1] + state.background.data[i * 4 + 2]) / 3;
		const grey = (frame.data[i * 4 + 0] + frame.data[i * 4 + 1] + frame.data[i * 4 + 2]) / 3;
		const result = (grey - bg) > 120 ? 255 : 0;

		frame.data[i * 4 + 0] = result;
		frame.data[i * 4 + 1] = result;
		frame.data[i * 4 + 2] = result;
	}

    oCtx.putImageData(frame, 0, 0);
}

snapshotButton.addEventListener("click", snapshot);

function scan() {
	const w = state.width;
	const h = state.height;

	slCtx.fillStyle = "black";
	slCtx.fillRect(0, 0, w, h);
	slCtx.fillStyle = "white";
	slCtx.fillRect(0, state.y, w, state.lineWidth);


	// write this to popup window
	const nwd = state.scanWindow.document;
	const nwdContainer = nwd.querySelector(".container");
    const nwdCanvas = nwd.querySelector(".scan-window");

    const { width, height } = nwdContainer.getBoundingClientRect();

    nwdCanvas.width = width;	
    nwdCanvas.height = height;
    const nwdCtx = nwdCanvas.getContext("2d");
    nwdCtx.fillStyle = "black";
	nwdCtx.fillRect(0, 0, nwdCanvas.width, nwdCanvas.height);
	nwdCtx.fillStyle = "white";
	nwdCtx.fillRect(0, state.y, w, state.lineWidth);

}

scanButton.addEventListener("click", scan)




