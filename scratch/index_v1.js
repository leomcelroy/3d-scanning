import {render, html, svg} from 'https://unpkg.com/uhtml?module';

const state = {
	width: 0,
	height: 0,
	background: null,
	y: 450,
	lineWidth: 10,
	download: false,
	threshold: 70,
	focalLength: 37,
	projectedWidth: 0,
	projectedHeight: 0,
	drawOverlay: false,
	camera: {
		focalLength: 1460,
		width: 1920,
		height: 1080,
	},
	projector: {
		focalLength: 1750,
		width: 1280,
		height: 720,
	}
}

const view = (state) => html`
	<div class="canvas-containers">
		<video id="video" width="1920"></video>
		<canvas id="overlay-canvas" @click=${getMouseCoordinates}></canvas>
		<canvas id="bg-canvas"></canvas>
		<canvas id="output-canvas"></canvas>
	</div>
	<div class="toolbox">
		<button @click=${() => start()}>init video</button>
		<button @click=${() => initCanvas()}>init canvas</button>
		<button @click=${() => crosses()}>draw crosses</button>
		<button @click=${() => verticalLines()}>vertical lines</button>
		<button @click=${() => scan()}>scan</button>
		<button @click=${() => snapshot(state)}>snapshot</button>
		<span>
			Y-Value:  <input 
				type="number" 
				@change=${(e) => changeY(e)}
				.value=${state.y}/>
		</span>
		<span>
			Download: <input 
				type="checkbox" 
				@change=${() => {state.download = !state.download} } 
				.value="${state.download}"/>
		</span>
		<span>
			Focal Length: <input 
				type="number" 
				@change=${(e) => {state.focalLength = Number(e.target.value)} } 
				.value="${state.focalLength}"/>
		</span>
		<button @click=${() => scanDownload()}>scan and download</button>
		<button @click=${() => getHeight()}>process</button>
	</div>
`

render(document.body, view(state));

function getMouseCoordinates(e) {
	const canvas = document.querySelector("#overlay-canvas");
	const rect = canvas.getBoundingClientRect();
  const x = e.clientX - rect.left;
  const y = e.clientY - rect.top;
  console.log(x, y);
  return {x, y}
}

const video = document.getElementById('video');
const image = document.getElementById("image");

const bgCanvas = document.querySelector("#bg-canvas");
const outputCanvas = document.querySelector("#output-canvas");
const overlayCanvas = document.querySelector("#overlay-canvas");
const bgCtx = bgCanvas.getContext("2d");
const oCtx = outputCanvas.getContext("2d");
const overlayCtx = overlayCanvas.getContext("2d");


function sleep(ms = 0) {
  return new Promise(r => setTimeout(r, ms));
}


function downloadImg(name) {
	var imageData = outputCanvas.toDataURL();

    // create temporary link  
    var tmpLink = document.createElement('a');  
    tmpLink.download = `${name}.png`; // set the name of the download file 
    tmpLink.href = imageData;  
      
    // temporarily add link to body and initiate the download  
    document.body.appendChild( tmpLink );  
    tmpLink.click();  
    document.body.removeChild( tmpLink );
}


const start = async () => {
	
	const stream = await initVideo();

	console.log(window.webkitURL)

	if (false) {
        stream = window.webkitURL.createObjectURL(stream);
        state.stream = stream;
        video.src = stream;
		video.srcObject = stream;
    } else {
        state.stream = stream;
        video.src = stream;
		video.srcObject = stream;
    }

    await sleep(500);

    const tracks = stream.getVideoTracks();
	const track = tracks[0];

	console.log(navigator.mediaDevices.getSupportedConstraints());
	// const tracks = stream.getVideoTracks();
	const capabilities = track.getCapabilities();
	const settings = track.getSettings();
	console.log({track, capabilities, settings});

	// track.applyConstraints({
	//     advanced: [{
	//        focusMode: "manual",
	//     }]
 //    })

	setUpBackground();
}

function setUpBackground() {
	// open pop up window
	const html = `
		<style>
			html, body {
				margin: 0px;
			}

			.container {
				width: 100vw;
				height: 100vh;
			}
		</style>
		<div class="container">
			<canvas class="scan-window"></canvas>
		</div>
	`

	state.scanWindow = window.open("", "", "width=200, height=200");
	const nwd = state.scanWindow.document;
	nwd.open()
    nwd.write(html)
    nwd.close()
}

function drawCross(x, y, ctx) {
	ctx.strokeStyle = "white";
  ctx.beginPath();

  ctx.moveTo(x - 15, y);
  ctx.lineTo(x + 15, y);

  ctx.moveTo(x, y - 15);
  ctx.lineTo(x, y + 15);
  ctx.lineWidth = 4;
  ctx.stroke();
}

function drawCrosses(w, h, ctx) {
	const width = w*.75;
	drawCross(w/2 - width/2, h/2 + width*7/10/2, ctx);
	drawCross(w/2 - width/2, h/2 - width*7/10/2, ctx);
	drawCross(w/2 + width/2, h/2 + width*7/10/2, ctx);
	drawCross(w/2 + width/2, h/2 - width*7/10/2, ctx);
}

function drawVerticalLines(w, h, ctx) {
	ctx.fillStyle = "black";
	ctx.fillRect(0, 0, w, h);
	ctx.strokeStyle = "white";
  ctx.beginPath();
  ctx.moveTo(w*.1, 0);
  ctx.lineTo(w*.1, h);

  ctx.moveTo(w*.9, 0);
  ctx.lineTo(w*.9, h);
  ctx.lineWidth = 10;
  ctx.stroke();
}

function verticalLines() {
	state.drawOverlay = true;
	const nwd = state.scanWindow.document;
	const nwdContainer = nwd.querySelector(".container");
  const nwdCanvas = nwd.querySelector(".scan-window");
  const nwdCtx = nwdCanvas.getContext("2d");

  const { width, height } = nwdContainer.getBoundingClientRect();
  nwdCanvas.width = width;	
  nwdCanvas.height = height;

  drawVerticalLines(width, height, nwdCtx);
}

function crosses() {
	state.drawOverlay = false;
	const nwd = state.scanWindow.document;
	const nwdContainer = nwd.querySelector(".container");
  const nwdCanvas = nwd.querySelector(".scan-window");


  const { width, height } = nwdContainer.getBoundingClientRect();

  nwdCanvas.width = width;	
  nwdCanvas.height = height;
  const nwdCtx = nwdCanvas.getContext("2d");
  nwdCtx.fillStyle = "black";
	nwdCtx.fillRect(0, 0, nwdCanvas.width, nwdCanvas.height);

	drawCrosses(width, height, nwdCtx);
	state.projectedWidth = width;
	state.projectedHeight = height;
}
 
function initBG() {
	const nwd = state.scanWindow.document;
	const nwdContainer = nwd.querySelector(".container");
  const nwdCanvas = nwd.querySelector(".scan-window");


  const { width, height } = nwdContainer.getBoundingClientRect();

  nwdCanvas.width = width;	
  nwdCanvas.height = height;
  const nwdCtx = nwdCanvas.getContext("2d");
  nwdCtx.fillStyle = "black";
	nwdCtx.fillRect(0, 0, nwdCanvas.width, nwdCanvas.height);

	state.projectedWidth = width;
	state.projectedHeight = height;

	console.log("projected", width, height)
}

function drawOverlay() {
	const ctx = overlayCtx;
	const w = overlayCanvas.width;
	const h = overlayCanvas.height;
	ctx.drawImage(video, 0, 0, w, h);

	if (state.drawOverlay) {
		ctx.strokeStyle = "red";
	  ctx.beginPath();
	  const ratio0 = state.camera.focalLength/state.camera.width;
	  const ratio1 = state.projector.focalLength/state.projector.width;
	  const dx = w*.1*ratio1/ratio0;
	  ctx.moveTo(dx, 0);
	  ctx.lineTo(dx, h);

	  ctx.moveTo(w-dx, 0);
	  ctx.lineTo(w-dx, h);
	  ctx.lineWidth = 10;
	  ctx.stroke();
	}


	window.requestAnimationFrame(drawOverlay);
}

async function initCanvas() {
	const { width, height } = video.getBoundingClientRect();
	bgCanvas.width = width;
	bgCanvas.height = height;
	outputCanvas.width = width;
	outputCanvas.height = height;
	overlayCanvas.width = width;
	overlayCanvas.height = height;
	state.width = width;
	state.height = height;

	initBG();
	drawOverlay();

	await sleep(500);
	bgCtx.drawImage(video, 0, 0, state.width, state.height);
	state.background = bgCtx.getImageData(0, 0, state.width, state.height);
	snapshot(state);
}

function snapshot({ y, download, width, height, threshold }) {
	oCtx.drawImage(video, 0, 0, width, height);
	const frame = oCtx.getImageData(0, 0, width, height);

	var l = frame.data.length / 4;

	for (var i = 0; i < l; i++) {
		const bg = (state.background.data[i * 4 + 0] + state.background.data[i * 4 + 1] + state.background.data[i * 4 + 2]) / 3;
		const grey = (frame.data[i * 4 + 0] + frame.data[i * 4 + 1] + frame.data[i * 4 + 2]) / 3;
		const result = (grey - bg) > threshold ? 255 : 0;

		frame.data[i * 4 + 0] = result;
		frame.data[i * 4 + 1] = result;
		frame.data[i * 4 + 2] = result;
		// frame.data[i * 4 + 3] = result;


		// frame.data[i * 4 + 0] = 0;
		// frame.data[i * 4 + 1] = 0;
		// frame.data[i * 4 + 2] = 0;
		// frame.data[i * 4 + 3] = result;
	}

    oCtx.putImageData(frame, 0, 0);
    if (download) downloadImg(`${y}-y-scan`);
}

const gaussianFunc = (val, mean, stdev) => {
	const delta = val-mean;
	const exponent = (delta/stdev)**2;
	const gaussian = Math.exp(-exponent/2)

	return [0, 0, 0, 255 - Math.round(gaussian*255)];
}

const fillBuffer = (width, height, func) => {
	const length = width*height*4;
	let buffer = new Uint8ClampedArray(length);

	for (let i = 0; i < length; i += 4) {
		const x = i/4%width;
		const y = Math.floor(i/4/width);

		const [r, g, b, a] = func(x, y);


		buffer[i] = r;
		buffer[i + 1] = g;
		buffer[i + 2] = b;
		buffer[i + 3] = a;
	}

	return buffer;
}


function scan(y) {
	// write this to popup window
	const nwd = state.scanWindow.document;
	const nwdContainer = nwd.querySelector(".container");
  const nwdCanvas = nwd.querySelector(".scan-window");

  const { width, height } = nwdContainer.getBoundingClientRect();

  nwdCanvas.width = width;	
  nwdCanvas.height = height;
  const nwdCtx = nwdCanvas.getContext("2d");
  nwdCtx.fillStyle = "black";
	nwdCtx.fillRect(0, 0, nwdCanvas.width, nwdCanvas.height);
	nwdCtx.fillStyle = "white";
	// draw rectangle
	// nwdCtx.fillRect(0, y - state.lineWidth/2, nwdCanvas.width, state.lineWidth);

	const stdev = 10;
	drawGaussian(nwdCanvas, y, width, height, stdev, stdev*5);

}

// frep -> gerber -> osh park
// stencil font

//////////////////////////


/// test gaussian
let y = 300;
let w = 600;
let h = 600;
let std = 50;
let gaussHeight = std*5

drawGaussian(outputCanvas, y, w, h, std, gaussHeight);

function drawGaussian(canvas, y, w, h, stdev, gaussHeight) {
	canvas.width = w;
	canvas.height = h;
	const ctx = canvas.getContext("2d");

	ctx.fillStyle = "black";
	ctx.fillRect(0, 0, w, h);

	const b = fillBuffer(w, gaussHeight, (x, yPos) => {
		return gaussianFunc(yPos+y-gaussHeight/2, y, stdev);
	});

	console.log(b);
	const img = new ImageData(b, w);
	ctx.putImageData(img, 0, y - gaussHeight/2);	
}


async function changeY(e) {
	state.y = Number(e.target.value);
	scan(state.y);
	await sleep(500);
	snapshot(state);
}



async function scanDownload(e) {
	const pts = [];
	for (let i = 50; i < 500; i++) {
		scan(i);
		await sleep(50);
		state.y = i;
		snapshot(state);
		pts.push(...getHeight());
		// snapshot({ ...state, y: i });
	}

	const n = pts.length;

	const ply = `ply
format ascii 1.0
element vertex ${n}
property float32 x
property float32 y
property float32 z
end_header
${pts.map(([x, y, z]) => `${x} ${y} ${z}`).join("\n")}
`

	console.log(ply);
}

function getMeanHeight(arr) {
	let possiblePixels = [];

	for (let i = 0; i < arr.length; i++) {
		const px = arr[i];

		if (possiblePixels.length === 0) possiblePixels.push(px);
		else if (possiblePixels[possiblePixels.length - 1] - px === 1) possiblePixels.push(px);
		else if (possiblePixels.length >= 4) break;
		else possiblePixels = [];
	}

	return possiblePixels.length >= 4 ? possiblePixels.reduce((acc, cur) => acc + cur, 0)/possiblePixels.length : 0;
}

function getHeight() {
	var image = oCtx.getImageData(0, 0, outputCanvas.width, outputCanvas.height);
	var l = image.data.length / 4;
	const rows = image.height;
	const cols = image.width;
	// let test = new Uint8ClampedArray(cols*rows*4);
	const whitecounts = [];
	for (let i = 0; i < cols; i++) {
		let whitecount = [];
		for (let j = rows-1; j >= 0; j--) {
			const index = (i+j*cols)*4

			const px = image.data[index+1];

			// test[index] = image.data[index]
			// test[index + 1] = image.data[index + 1]
			// test[index + 2] = image.data[index + 2]
			// test[index + 3] = image.data[index + 3]
			// image.data[i+j*rows] = 45
			if (px === 255) whitecount.push(j);
		}

		whitecounts.push(getMeanHeight(whitecount));
		
	}	

	// const newImg = new ImageData(test, cols);
	// console.log(newImg, whitecounts);

	// outputCanvas.width = cols;
	// outputCanvas.height = rows;
	// oCtx.putImageData(newImg, 0, 0)

	const points = [];

	oCtx.fillStyle = "red";
	for (let i = 0; i < whitecounts.length; i++) {
		if (whitecounts[i] === 0) continue;
		oCtx.beginPath();
		oCtx.arc(i, whitecounts[i], 2, 0, 2 * Math.PI);
		oCtx.fill();
		const x = i - (image.width-1)/2;
		const y = whitecounts[i] - (image.height-1)/2;
		const norm = Math.sqrt(x**2+y**2+state.camera.focalLength**2);
		points.push([ x/norm, y/norm, state.camera.focalLength/norm ]);
	}

	const projectorPt = [ 0, state.y - state.projectedHeight/2, state.projector.focalLength ];
	const projectorPtNorm = Math.sqrt(projectorPt[0]**2+projectorPt[1]**2+projectorPt[2]**2);
	const projectorPtNormal = [ 
		0/projectorPtNorm, 
		(state.y - state.projectedHeight/2)/projectorPtNorm, 
		state.projector.focalLength/projectorPtNorm
	];	



	const cameraPos = [7.14586212, -81.71286646, 76.40578868];


	// const cameraPos = [0, -81.71286646, 0];
	// what about camera rotation?
	const realSpacePts = points.map((u, i) => {
		// console.log({i, cameraPos, u, projectorPtNormal})
		return isect_line_plane_v3(cameraPos, u, [0, 0, 0], projectorPtNormal, [1, 0, 0]);
	});

	return realSpacePts;
	// for (var i = 0; i < l; i++) { // row by row
	// 	const r = image.data[i * 4 + 0];
	// 	const g = image.data[i * 4 + 1];
	// 	const b = image.data[i * 4 + 2];
	// 	const a = image.data[i * 4 + 3];
	// 	// frame.data[i * 4 + 0] = result;
	// 	// frame.data[i * 4 + 1] = result;
	// 	// frame.data[i * 4 + 2] = result;
	// }

}

function crossProduct(v0, v1) {
	return [
		v0[1]*v1[2] - v0[2]*v1[1],
		-(v0[0]*v1[2] - v0[2]*v1[0]),
		v0[0]*v1[1] - v0[1]*v1[0],
	]
}

function isect_line_plane_v3(cameraPos, cameraNormal, planePos, planeNormal0, planeNormal1, epsilon=1e-6) {
	const crossNormals = crossProduct(planeNormal0, planeNormal1);
	const numerator = dot(crossNormals, sub(cameraPos, planePos))
	const denominator = dot(neg(cameraNormal), crossNormals);
	const t = numerator/denominator;

	return add(cameraPos, mul(cameraNormal, t));
}


function add(v0, v1) {
    return [
        v0[0] + v1[0],
        v0[1] + v1[1],
        v0[2] + v1[2],
    ]

}

function neg(v) {
    return v.map(x => -x);
}

function sub(v0, v1) {
    return [
        v0[0] - v1[0],
        v0[1] - v1[1],
        v0[2] - v1[2],
    ]

}

function dot(v0, v1) {
    return (
        (v0[0] * v1[0]) +
        (v0[1] * v1[1]) +
        (v0[2] * v1[2])
    )
}


function len_squared(v0) {
    return dot_v3v3(v0, v0)
}


function mul(v0, f) {
    return [
        v0[0] * f,
        v0[1] * f,
        v0[2] * f,
    ]
}

// all in pixel units
const makeMatrix = ({ focalLength, width, height }) => [
	[focalLength, 0, (width-1)/2],
	[0, focalLength, (height-1)/2],
	[0, 0, 1]
]

const cameraMatrix = makeMatrix(state.camera);
const projectorMatrix = makeMatrix(state.projector);


