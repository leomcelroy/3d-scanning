const initStream = async () => {
	let videoConstraints = {
	  frameRate: 20,
	  width: 1920,
	  height: 1080,
	};

	const mediaDevices = await navigator.mediaDevices;
	const devices = (await mediaDevices.enumerateDevices()).filter( x => x.label.includes("USB Camera"));

	return mediaDevices.getUserMedia({
	  video: videoConstraints,
	  audio: false
	})
}

export const initVideoCamera = async (videoEl) => {
	videoEl.setAttribute('autoplay', '');
	videoEl.setAttribute('muted', '');
	videoEl.setAttribute('playsinline', '');


	const stream = await initStream();

	// console.log(window.webkitURL)

	if (false) {
        stream = window.webkitURL.createObjectURL(stream);
        videoEl.src = stream;
		videoEl.srcObject = stream;
    } else {
        videoEl.src = stream;
		videoEl.srcObject = stream;
    }

    // set constraints here

 	// const tracks = stream.getVideoTracks();
	// const track = tracks[0];

	// console.log(navigator.mediaDevices.getSupportedConstraints());
	// const capabilities = track.getCapabilities();
	// const settings = track.getSettings();
	// console.log({track, capabilities, settings});

	// track.applyConstraints({
	//     advanced: [{
	//        focusMode: "manual",
	//     }]
 	//    })

 	return stream;
}