export function downloadImg(name, canvas) {
	var imageData = canvas.toDataURL();

    // create temporary link  
    var tmpLink = document.createElement('a');  
    tmpLink.download = `${name}.png`; // set the name of the download file 
    tmpLink.href = imageData;  
      
    // temporarily add link to body and initiate the download  
    document.body.appendChild( tmpLink );  
    tmpLink.click();  
    document.body.removeChild( tmpLink );
}