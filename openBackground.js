export function openBackground() {
	// open pop up window
	const html = `
		<style>
			html, body {
				margin: 0px;
			}

			.container {
				width: 100vw;
				height: 100vh;
			}
		</style>
		<div class="container">
			<canvas class="scan-window"></canvas>
		</div>
	`

	const scanWindow = window.open("", "", "width=200, height=200");
	const nwd = scanWindow.document;
	nwd.open();
	nwd.write(html);
	nwd.close();

	return nwd;
}