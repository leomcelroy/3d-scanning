function drawCross(x, y, ctx) {
	ctx.strokeStyle = "white";
	ctx.beginPath();

	ctx.moveTo(x - 15, y);
	ctx.lineTo(x + 15, y);

	ctx.moveTo(x, y - 15);
	ctx.lineTo(x, y + 15);
	ctx.lineWidth = 4;
	ctx.stroke();
}

export function crosses(w, h, ctx) {
	ctx.fillStyle = "black";
	ctx.fillRect(0, 0, w, h);

	const width = w*.75;
	drawCross(w/2 - width/2, h/2 + width*7/10/2, ctx);
	drawCross(w/2 - width/2, h/2 - width*7/10/2, ctx);
	drawCross(w/2 + width/2, h/2 + width*7/10/2, ctx);
	drawCross(w/2 + width/2, h/2 - width*7/10/2, ctx);
}


export function verticalLines(w, h, ctx) {
	ctx.fillStyle = "black";
	ctx.fillRect(0, 0, w, h);

	ctx.strokeStyle = "white";
	ctx.beginPath();
	ctx.moveTo(w*.1, 0);
	ctx.lineTo(w*.1, h);

	ctx.moveTo(w*.9, 0);
	ctx.lineTo(w*.9, h);
	ctx.lineWidth = 10;
	ctx.stroke();
}


///////////////////

const gaussianFunc = (val, mean, stdev) => {
	const delta = val-mean;
	const exponent = (delta/stdev)**2;
	const gaussian = Math.exp(-exponent/2)

	return [0, 0, 0, 255 - Math.round(gaussian*255)];
}

export const fillBuffer = (width, height, func) => {
	const length = width*height*4;
	let buffer = new Uint8ClampedArray(length);

	for (let i = 0; i < length; i += 4) {
		const x = i/4%width;
		const y = Math.floor(i/4/width);

		const [r, g, b, a] = func(x, y);


		buffer[i] = r;
		buffer[i + 1] = g;
		buffer[i + 2] = b;
		buffer[i + 3] = a;
	}

	return buffer;
}

export function gaussian(ctx, y, w, h, stdev, gaussHeight) {
	ctx.fillStyle = "black";
	ctx.fillRect(0, 0, w, h);

	const b = fillBuffer(w, gaussHeight, (x, yPos) => {
		return gaussianFunc(yPos+y-gaussHeight/2, y, stdev);
	});

	const img = new ImageData(b, w);
	ctx.putImageData(img, 0, y - gaussHeight/2);	
}