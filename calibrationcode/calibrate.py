import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt

xcorners = 9
ycorners = 6

# scan = cv.imread('scan.jpg')
scan = cv.imread('scan_crop.jpg')

scan_gs = cv.cvtColor(scan, cv.COLOR_BGR2GRAY)
success, corners = cv.findChessboardCorners(scan_gs,
                                            (xcorners, ycorners),
                                            flags = cv.CALIB_CB_NORMALIZE_IMAGE)

# test = cv.drawChessboardCorners(scan, (xcorners, ycorners), corners, success)
# plt.imshow(test)
# cv.imwrite('no_subpixel.png', test)

corners_subpix = cv.cornerSubPix(scan_gs,
                                 corners,
                                 (22,22), (-1,-1),
                                 (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.001))

# test = cv.drawChessboardCorners(scan, (xcorners, ycorners), corners_subpix, success)
# plt.imshow(test)
# cv.imwrite('subpixel.png', test)

objcoords = np.zeros((ycorners*xcorners,3), np.float32)
objcoords[:,:2] = np.mgrid[0:xcorners,0:ycorners].T.reshape(-1,2)*26.125
corners_subpix = corners_subpix[np.newaxis, :, 0, :]
objcoords = objcoords[np.newaxis, :, :]
print(corners_subpix.dtype, corners_subpix.shape)
print(objcoords.dtype, objcoords.shape)

ret, mtx, dist, rvecs, tvecs = cv.calibrateCamera(objcoords, corners_subpix,
                                                  scan_gs.shape[::-1],
                                                  None,
                                                  None)


print(ret, mtx, dist, rvecs, tvecs)