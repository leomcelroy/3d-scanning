export function drawRaw(canvas, video, state) {
	const { width: w, height: h } = state.camera;
	canvas.width = w;
	canvas.height = h;

	const ctx = canvas.getContext("2d");

	const draw = (state) => {
		ctx.drawImage(video, 0, 0, canvas.width, canvas.height);

		if (state.overlay) {
			ctx.strokeStyle = "red";
			ctx.beginPath();
			const ratio0 = state.camera.focalLength/state.camera.width;
			const ratio1 = state.projector.focalLength/state.projector.width;
			const dx = w*.1*ratio1/ratio0;
			ctx.moveTo(dx, 0);
			ctx.lineTo(dx, h);

			ctx.moveTo(w-dx, 0);
			ctx.lineTo(w-dx, h);
			ctx.lineWidth = 10;
			ctx.stroke();
		}

		window.requestAnimationFrame(() => draw(state));
	}


	draw(state);
}