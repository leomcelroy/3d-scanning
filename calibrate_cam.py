import numpy as np
import cv2


def main():
    focalLength = 1460
    width = 1920
    height = 1080

    k = np.array([[focalLength, 0, (width-1)/2],
              [0, focalLength, (height-1)/2],
              [0, 0, 1]], dtype=np.float32)

    distort = np.array([0, 0, 0, 0, 0], dtype=np.float32)

    w = 100
    h = 70
    dist = 370

    objPoints = np.array([[[-w, -h, dist],
                      [w, -h, dist],
                      [-w, h, dist],
                      [w, h, dist]]], dtype=np.float32)

    # imgPoints = np.array([[[468.015625, 606.0],
    #              [1462.515625, 597.0234375],
    #              [475.015625, 1298.0234375],
    #              [1470.515625, 1300.0234375]]], dtype=np.float32)

#     456.015625 332.015625
# index.js:168 1420.015625 318.015625
# index.js:168 494.015625 970.015625
# index.js:168 1395.015625 963.015625
# 578 255
# index.js:168 1302 242
# index.js:168 608 737
# index.js:168 1286 721

# 597.015625 396.015625
# index.js:168 1325.015625 392.015625
# index.js:168 609.015625 879.015625
# index.js:168 1322.015625 877.015625

# 600.015625 388.015625
# index.js:173 1325.015625 374.015625
# index.js:173 611.015625 886.015625
# index.js:173 1321.015625 880.015625

    imgPoints = np.array([[[600.015625, 388.015625],
             [1325.015625, 374.015625],
             [611.015625, 886.015625],
             [1321.015625, 880.015625]]], dtype=np.float32)

    # camera_matrix = cv2.initCameraMatrix2D(objPoints, objPoints, (width, height))
    # print(camera_matrix)

    # return

    ret, mtx, err, rvecs, tvecs = cv2.calibrateCamera(objPoints, imgPoints, (width, height), k, None, flags=cv2.CALIB_USE_INTRINSIC_GUESS)
    print(ret)
    print(rvecs, tvecs)
    print(err)



if __name__ == "__main__":
    main()